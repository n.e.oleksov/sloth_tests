from nose.tools import assert_equals

from sloth.alfa.policy import Policy
from sloth.types import SlothAlgorithm


policy = """
namespace example {
    export policy mainPolicy {
        target clause action == "GET"
        apply denyUnlessPermit
        rule r11 {
            permit
            target clause any ["user", "admin"] in subject.roles 
        }
        rule r12 {
            permit
            target clause "admin" in subject.roles
        }
        rule r13 {
            permit
            target clause "admin" in subject.roles
                   clause subject.ip == "127.0.0.1"
        }
    }
}
"""


def test_base():
    p = Policy(policy)
    assert_equals(p.algorithm, SlothAlgorithm.DenyUnlessPermit)
