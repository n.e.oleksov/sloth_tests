from aule.codegen import ust
from nose.tools import assert_equals

from sloth.alfa.idl import IDL

interfaces = """
    interface Entity {
      abstract id: str;
    }

    [angine=entity]
    interface UrlEntity <: Entity {
      path: str;
      level: int;
      tags: [str];
    }

    [angine=subject]
    interface Subject <: Entity {
      name: str;
      roles: [str];
      level: int;
      tags: [str];
      is_admin: bool;
      abstract ip: str;
    }
"""


def test_base():
    i = IDL(interfaces)
    assert_equals(i.vars['urlentity']['path'].type, ust.SimpleType.STRING)
