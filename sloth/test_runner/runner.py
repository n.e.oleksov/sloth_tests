from angine.generation import generate_policy_runtime
from lupa import LuaRuntime

from .handlers import Handlers
from .decision import Decision


class Runner:
    def __init__(self, policy):
        lua = LuaRuntime()
        lua_code = generate_policy_runtime(policy)
        lua.execute(lua_code)
        self.actions = lua.table_from({
            e.name.lower(): e.value for e in Decision
        })

        for gname, gfunc in dict(lua.globals()).items():
            if gname.startswith("__main"):
                self.evaluate = gfunc
                break
        else:
            raise ValueError("ALFA policy entry point not found")

    def check(self, request, decision):
        d = Decision(
            self.evaluate(
                request, self.actions, Handlers()
            )
        )
        if decision != d:
            print("Fail", request, d, decision)
            return False
        else:
            print("Ok", request, d, decision)

            return True
