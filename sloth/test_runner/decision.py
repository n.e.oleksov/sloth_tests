from enum import IntEnum


class Decision(IntEnum):
    """ Decision described possible return values of the Lua runtime """
    Permit = 0
    Deny = 1
    NotApplicable = 2
    Indeterminate = 3


