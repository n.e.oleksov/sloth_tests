import z3

import aule

from dopefish.alfa.visitor import Visitor
from dopefish.alfa.type_helper import TypeHelper


def policy2constraint(policy, interfaces):
    alfa_policy = aule.ASTParserFactory \
        .create("alfa") \
        .parse(policy)
    namespace = alfa_policy.body[0]
    idl_interfaces = aule.ASTParserFactory \
        .create("idl") \
        .parse(interfaces)
    ast_visitor = Visitor(
        type_helper=TypeHelper(idl_interfaces)
    )
    return [
        ast_visitor.visit(p) for p in namespace.body
    ], ast_visitor.attributes.keys()


def true_constraint(targets):
    return z3.Or([t.evaluate().is_true() for t in targets])


def false_constraint(targets):
    return z3.Or([t.evaluate().is_false() for t in targets])


def policy_constraint(policy, conditions):
    target_condition = true_constraint(policy.targets)
    rules_condition = []
    for c, rule in zip(conditions, policy.rules):
        if c:
            rules_condition.append(true_constraint(rule.targets))
        else:
            rules_condition.append(false_constraint(rule.targets))
    return z3.And(
        target_condition,
        z3.And(rules_condition)
    )