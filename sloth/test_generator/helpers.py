import itertools
import z3

from sloth.constrains import policy_constraint
from sloth.test_generator import model2request
from .decisionmaker import DecisionMaker


def get_index_of_first_true(s):
    for i, j in enumerate(s):
        if j:
            return i
    else:
        return -1


def solve(constraint):
    solver = z3.Solver()
    solver.add(constraint)
    if solver.check().r == 1:
        return solver.model()
    return None


def generate_test(policy, attributes):
    dm = DecisionMaker(policy)
    test_suit = [list(tc) for tc in itertools.product((False, True), repeat=len(policy.rules))]

    test_set = []
    for test_case in test_suit:
        constraint = policy_constraint(policy, test_case)
        model = solve(constraint)
        if model is not None:
            test_set.append((
                model2request(model, attributes),
                dm.algorithm.decision(test_case)
            ))
    return test_set
