import z3


def extract_value(value):
    if isinstance(value, z3.z3.SeqRef):
        return value.as_string().replace('"', '')
    if isinstance(value, z3.z3.IntNumRef):
        return value.as_long()
    if isinstance(value, z3.z3.BoolRef):
        return bool(value)


def parse_atr_access(request_dict):
    new_dict = {}
    for i in request_dict:
        k = i.split('.')
        if len(k) > 1:
            if k[0] in new_dict:
                new_dict[k[0]].update({k[1]: request_dict[i]})
            else:
                new_dict[k[0]] = {k[1]: request_dict[i]}
        else:
            new_dict[str(i)] = request_dict[i]
    return new_dict


def key_to_str(dict):
    new_dict = {}
    for i in dict:
        new_dict[str(i)] = dict[i]
    return new_dict


def model2request(model, policy_var):
    request = {}
    for i in model:
        if str(i) in policy_var:
            if isinstance(model[i], z3.z3.FuncInterp):
                request[i] = []
                else_value = extract_value(model[i].else_value())
                for j in range(model[i].num_entries()):
                    index, value = model[i].entry(j).as_list()
                    index = index.as_long()
                    value = extract_value(value)

                    try:
                        request[i][index] = value
                    except IndexError:

                        while len(request[i]) != index:
                            request[i].append(else_value)
                        request[i].append(value)
                if not bool(request[i]):
                    request[i].append(else_value)
            else:
                request[i] = extract_value(model[i])

    policy_var = list(policy_var)
    request = key_to_str(request)

    for i in policy_var:
        if i not in request.keys():
            request[i] = 42

    return parse_atr_access(request)
