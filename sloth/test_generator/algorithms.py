from abc import abstractmethod

from dopefish.constraints import Effect
from sloth.test_generator import helpers
from sloth.test_runner import Decision


class SlothBaseAlgorithm:
    def __init__(self, policy):
        self.rules = policy.rules

    @abstractmethod
    def decision(self, test_case):
        pass


class SlothFirstApplicable(SlothBaseAlgorithm):
    def decision(self, test_case):
        index = helpers.get_index_of_first_true(test_case)
        if index == -1:
            return Decision.NotApplicable
        elif self.rules[index].effect == Effect.PERMIT:
            return Decision.Permit
        elif self.rules[index].effect == Effect.DENY:
            return Decision.Deny
        else:
            return Decision.Indeterminate


class SlothPermitUnlessDeny(SlothBaseAlgorithm):
    def decision(self, test_case):
        d = Decision.Permit
        for i, j in enumerate(test_case):
            if j:
                if self.rules[i].effect == 'deny':
                    d = Decision.Deny
                    break
        return d


class SlothDenyUnlessPermit(SlothBaseAlgorithm):
    def decision(self, test_case):
        d = Decision.Deny
        for i, j in enumerate(test_case):
            if j:
                if self.rules[i].effect == Effect.PERMIT:
                    d = Decision.Permit
                    break
        return d


class SlothPermitOverrides(SlothBaseAlgorithm):
    def decision(self, test_case):
        d = Decision.Indeterminate
        for i, j in enumerate(test_case):
            if j:
                if self.rules[i].effect == Effect.PERMIT:
                    d = Decision.Permit
                elif d == Decision.Indeterminate:
                    d = Decision.Deny
        return d


class SlothDenyOverrides(SlothBaseAlgorithm):
    def decision(self, test_case):
        d = Decision.Indeterminate
        for i, j in enumerate(test_case):
            if j:
                if self.rules[i].effect == Effect.DENY:
                    d = Decision.Deny
                elif d == Decision.Indeterminate:
                    d = Decision.Permit
        return d


class SlothOnlyOneApplicable(SlothBaseAlgorithm):
    def decision(self, test_case):
        return Decision.Indeterminate
