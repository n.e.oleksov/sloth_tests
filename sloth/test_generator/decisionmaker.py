from dopefish.constraints import *
from sloth.test_generator.algorithms import *


class DecisionMaker:
    def __init__(self, policy):
        algorithm = policy.algorithm
        if algorithm == FirstApplicable:
            self.algorithm = SlothFirstApplicable(policy)
        elif algorithm == PermitUnlessDeny:
            self.algorithm = SlothPermitUnlessDeny(policy)
        elif algorithm == DenyUnlessPermit:
            self.algorithm = SlothDenyUnlessPermit(policy)
        elif algorithm == PermitOverrides:
            self.algorithm = SlothPermitOverrides(policy)
        elif algorithm == DenyOverrides:
            self.algorithm = SlothDenyOverrides(policy)
        elif algorithm == OnlyOneApplicable:
            self.algorithm = SlothOnlyOneApplicable(policy)
