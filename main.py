from sloth.test_runner import Runner
from sloth.constrains import policy2constraint
from sloth.test_generator import generate_test

idl_interfaces = """
    interface Entity {
      abstract id: str;
    }

    [angine=entity]
    interface UrlEntity <: Entity {
      path: str;
      tags: [str];
    }

    [angine=subject]
    interface Subject <: Entity {
      age: int;
      is_admin: bool;
    }
"""

alfa_policy = """
namespace example {
    export policy mainPolicy {
        target clause action == "GET"
        apply FirstApplicable
        rule r11 {
            permit
            target clause entity.path == "/shared" and "low" in entity.tags
        }
        rule r12 {
            deny
            target clause subject.age < 18 or subject.is_admin == false 
        }
    }
}
"""

if __name__ == "__main__":
    policies, attributes = policy2constraint(alfa_policy, idl_interfaces)
    runtime = Runner(alfa_policy)
    tests = generate_test(policies[0], attributes)
    for test in tests:
        runtime.check(test[0], test[1])
